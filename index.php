<?php 
include("config/db.php"); 
include("core/functions.php"); 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Sticky Footer Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Portfolio</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">All works</a></li>
            <li><a href="cliets.html">Clients</a></li>
            <li><a href="orders.html">Orders</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="singin.html">Exit</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <!-- Begin page content -->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <?php allworks($con); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-body">
                      <form role="form" enctype="multipart/form-data" id="form" method="post" action="ajax.php">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Название работы</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Введите название">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Описание</label>
                            <input type="text" name="desc"  class="form-control" id="exampleInputPassword1" placeholder="Введите описание">
                          </div>
                          <div class="form-group">
                            <div class="form-group">
                                <label for="image">File input</label>
                                <input id="image" name="image" type="file">
                                <p class="help-block">Example block-level help text here.</p>
                              </div>
                          </div>
                          <button id="subform" type="submit" class="btn btn-default">Добавить работу</button>
                        </form>

                  </div>
                </div>
            </div>
        </div>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">Portfolio / 2014</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
