<?php
function allworks($con) {
    $result = mysqli_query($con,"SELECT * FROM works");

    while($row = mysqli_fetch_array($result))
      {
      $id = $row['id_work'];
      $name = $row['name_work'];
      $desc = $row['desc_work'];
      $image = $row['images'];
      echo <<<HERE
        <div class="col-md-4">
                        <div class="panel panel-default">
                          <div class="panel-body" idwork="$id">
                            <a href="#" class="thumbnail">
                              <img data-src="holder.js/100%x180" src="img/wookmark.jpg" alt="$name">
                            </a>
                            <h3>$name</h3>
                            <p><small>$desc</small></p>
                            <p>
                            <button type="button" class="btn btn-default btn-sm">
                              <span class="glyphicon glyphicon-edit"></span> Edit
                            </button>  
                            </p>
                          </div>
                        </div>
                    </div> 
HERE;
      }

    mysqli_close($con);
}
function insertwork($con, $name, $desc) {
    mysqli_query($con,"INSERT INTO works (name_work, desc_work)
    VALUES ('".$name."', '".$desc."')");
    mysqli_close($con);   
}
?>